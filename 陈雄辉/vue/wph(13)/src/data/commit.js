export default {
    name:'commit',
    goods:[
        {id:1,name:'Air Jordan 1 Utility ，采用结实帆布、暗袋和拉链口袋设计，铸就 AJ1 的实用迭代更新之作',price:'1399', img:'../../男子运动鞋.jpg',num:1},
        {id:2,name:'Jordan Sport DNA男子T恤传承品牌经典引领街头服饰迈向未来',price:'259',img:'../../男子t恤.jpg',num:1},
        {id:3,name:'Nike Dri-FIT DNA 男子篮球短裤助你蓄势待发，在球场上发挥上佳表现',price:'299',img:'../../男子篮球短裤.jpg',num:1},
        {id:4,name:'玩转 Nike Everyday All-Court 8P 印花篮球，无论是室内还是户外球场，尽情秀出球技',price:'200',img:'../../印花篮球.jpg',num:1},
        {id:5,name:'商品详情弹力蕾丝紧身剪裁弹性裤腰连袜110毫米足弓意大利制造',price:'26900',img:'../../巴黎世家丝袜.webp',num:1}
      ],
      cart:[],
      addToCart:function(id){
        let inCart = false;
        this.cart.forEach(el=>{
            if(id==el.id){
                inCart=true;
                el.num++;
            }
        })
        if(inCart==false){
            this.goods.forEach(el=>{
              if(el.id==id){
                  let good =el;
                  good.num=1;
                  this.cart.push(good);
              }
            })          
        }
        console.log(this.cart)
},
getCart(){
    return this.cart;
   
},   
}
