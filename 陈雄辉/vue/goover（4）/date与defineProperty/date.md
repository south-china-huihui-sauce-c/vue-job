**date**

创建对象 new Date();不给参数，返回的是当前的事件对象，如果给定了参数，会返回改参数的对象

四种构造方法

new Date();//空参数
new Date(value);//时间戳
new Date(dateString);//日期格式
new Date(year, monthIndex [, day [, hours [, minutes [, seconds [, milliseconds]]]]]);

什么是时间戳？

```
//获取时间戳，返回毫秒数
new Date().getTime();

//获取我们看得懂时间的字符串


```

**vue mvvm的核心原理**

Object.defineProperty 
方法会直接在一个对象上定义一个新属性，或者修改一个对象的现有属性，并返回此对象。

```


